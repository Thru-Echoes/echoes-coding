# Icy Coding Practice 

![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

This website provides **the iciest** coding practices for collaborative data-driven / analytical research projects and software development. 

Documentation can be [found here (*link*)](https://thru-echoes.gitlab.io/echoes-coding/).

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [I. Software Carpentry: Documention Station!!](#i-software-carpentry-documention-station)
- [II. Documentation is High Priority!](#ii-documentation-is-high-priority)
  - [II.a "Woah - way too much writing / many comments!"](#iia-woah---way-too-much-writing--many-comments)
  - [II.b Levels of Documentation Details](#iib-levels-of-documentation-details)
- [III. README.md Documentation](#iii-readmemd-documentation)
- [IV. Notebook Documentation](#iv-notebook-documentation)
- [V. Function Documentation](#v-function-documentation)
- [VI. Git: Commits and Branches](#vi-git-commits-and-branches)
  - [Master branch: Oliver controls exclusively](#master-branch-oliver-controls-exclusively)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## I. Software Carpentry: Documention Station!!

Designing and implementing analytical workflows and software development is often a **complect** task in collaborative research projects. 

*Note: "complect" is defined as an object with components that are highly intertwined.* For example, our research project involves stitching together various data sources, statistical methods, and code sources that are highly complect (*...or complectity?...*).

Our tasks for this / any research project should be highly **accessible, reusable, and readable** code sources and workflows.

---

## II. Documentation is High Priority!

Cannot stress this enough...

### II.a "Woah - way too much writing / many comments!"

**WRONG!** 

*Over-documenting is never a problem.* 

*Under-documenting is always a problem.*  

### II.b Levels of Documentation Details

1. README.md: most general overview of code and workflow 
1. Notebooks: more specific, ~abstract of each at top 
1. Functions: even more specific, readable comments
1. Git commits: *$ git commit -m "most specific docs"*

---

## III. README.md Documentation

Brief description of each workflow step + Notebooks. 

1. Describe project logic 
1. Describe (briefly) each Notebook and code files

<br>

We need the least number of Notebooks and code files / sources that can subjectively described as high-level (*conceptual*) steps. 

For example, preprocessing and exploratory statistics of data can be logically grouped as a single notebook that **does not do anything past preprocessing and exploratory statistics**.

---

## IV. Notebook Documentation 

Each level of documentation should attempt to produce a file (-or- section of code software) that could be understood without looking at the rest of the code and workflow developed and explain what the general reasoning is behind the step. 

The top of each Notebook should be a sort of abstract of what the Notebook is for and what step in the workflow of our research project. And, in the same sense, visualization functions that are used across many Notebooks should be contained within a single **visualization.py** file within the *icyutils Python package* - see below for more information on software packages and their structure.

---

## V. Function Documentation 

**Docstrings!!** 

Docstrings are comments that exist just under the definition of a function:

 ```
        def this_fn(arg1, arg2):
            """
                Returns a list of strings based on arg1 x arg2
                        arg1 | <data_type> e.g. String, List, DataFrame
                        arg2 | <data_type>
            """
            return list(arg1 x arg2)

``` 

---

## VI. Git: Commits and Branches

**See "Gitcmds" for git commands and examples of switching branches.**

### Master branch: Oliver controls exclusively  

It will be too complicated / time-consuming to handle merge conflicts and all kinds of wonkiness if multiple developers are in parallel versions of dev functions, files, Notebooks, etc. 

The **master** branch will have the most conservative number of *commits* - only using it to push clean steps in developing the code sources / files / repo. Or pushes to *master* will be when a significant portion of the analytical workflow / results are obtained.

Think of the **master** branch as a *front-facing / user-facing* product that is being developed, e.g. we are a buisness (research team) creating a product (data analysis and curation). Thus, **only finished code + polished code / documentation is pushed as a commit to master branch.** 

**1) When switching between branches...** 

Entire repo (*i.e. all folders and files within*) changes to whatever the state of the last commit on that branch is / was.

**2) Looking at the history of commits...**

The top most commit showing is the last commit that was made to "dev" branch. *I.e. Commit "Nonsense to example.txt!"* And the next commit is whatever the last commit to master is / was since "dev" branch was made from "master" branch.  

**3) The history of commits on "trial-dev" branch...** 

Top most: "Testing prototype code..."
Next: "Nonsense to example.txt!" (since "trial-dev" branch was made from "dev" branch)
Next: "Last commit to master..." (since "dev" branch was made from "master" branch) 