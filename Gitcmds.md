# Notebook Workflows

This website provides **the iciest** coding practices for collaborative data-driven / analytical research projects and software development. 

Documentation can be [found here (*link*)](https://thru-echoes.gitlab.io/echoes-coding/Notebooks.md).

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [I. Git: command line version control](#i-git-command-line-version-control)
- [II. git commit](#ii-git-commit)
- [III. git branch](#iii-git-branch)
- [IV. Typical git workflow](#iv-typical-git-workflow)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## I. Git: command line version control 

See below for a demo of a typical workflow using git commands for staging changes, committing changes, switching branches. 

## II. git commit

```
        $ git commit -m "commit msgs are the most specific docs (data rates and charges may apply)"
```

<br>

## III. git branch

Branches are an **extremely icy** component of the *git* software. 

**"master" is the default branch each repo gets.** A branch is like a single clone of the entire repo (*i.e. all files, Notebooks, code, etc*).

If you are on the **master** branch, creating a new branch will clone the project at whatever state (**i.e. current commit of master branch**) it is in and become a parallel version of the repo. 

```
        # "git checkout -b <new-branch>"
        # Creating a new branch called "data" cloned from "master" branch 
        $ git checkout -b data master 
```

Branches can be cloned (**i.e. created**) from any other branch and will be created from the last commit of the branch being *cloned from.*

```
        # Creating a new branch called "noaa-dev" from "data" branch
        $ git checkout -b noaa-dev data
``` 

## IV. Typical git workflow

Here a demo of a typical git workflow: 

```
        # See what branch you are currently on and changes to any file(s)
        # "git status" will show "On branch master" by default 
        $ git status

        # Create new branch "dev" from latest commit on master branch 
        $ git checkout -b dev master 

        # After making changes to file "example.txt" 
        # (note: "git status" will now show "modified: example.txt" in red)
        # "git status" will show "On branch dev"  
        $ git status
        $ git add example.txt

        # Now "example.txt" will be shown in green under changes staged  
        $ git status  

        # Commit change (with message) to branch "dev"
        # (note: a commit is only local, does not update repo on GitHub / GitLab)
        $ git commit -m "Nonsense to example.txt!" 

        # Push commit and newly created "dev" branch to centralized repo 
        # I.e. push the change to GitHub / GitLab repo
        # This is the only way anyone else could look at your newly created "dev" branch
        # or see the commit you just made to it! 
        $ git push origin dev 

        # Create new branch "trial-dev" from "dev" branch 
        $ git checkout -b trial-dev dev 

        # Make a new file "trial-file.txt"
        $ touch trial-file.txt 
        $ git add trial-file.txt 
        $ git commit -m "Testing prototype code..." 
        $ git push origin trial-dev 

        # 1) Switch back to "dev" branch 
        $ git checkout dev 

        # 2) See the history of commits for "dev" branch 
        $ git log

        # 3) See the history of commits for "trial-dev" branch
        $ git checkout trial-dev  
        $ git log 
        
```


