# Notebook Workflows

This website provides **the iciest** coding practices for collaborative data-driven / analytical research projects and software development. 

Documentation can be [found here (*link*)](https://thru-echoes.gitlab.io/echoes-coding/Notebooks.md).

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [I. Organizations of Jupyter Notebooks](#i-organizations-of-jupyter-notebooks)
  - [I.i Complete Workflow Sequences](#ii-complete-workflow-sequences)
  - [I.ii Data Curation](#iii-data-curation)
  - [I.iii Analysis](#iiii-analysis)
- [II. Data Curation](#ii-data-curation)
  - [I.i signature Analysis](#ii-signature-analysis)
  - [I.ii Typologies Analysis](#iii-typologies-analysis)
- [III. Analysis](#iii-analysis)
- [IV. Markdown Examples](#iv-markdown-examples)
  - [IV.i Graphs using Mermaid Markdown](#ivi-graphs-using-mermaid-markdown)
  - [IV.ii Task Lists](#ivii-task-lists)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## I. Organizations of Jupyter Notebooks 

The organization of the Notebooks is in (*somewhat*) linear order.

### I.i Complete Workflow Sequences 

1. Data Curation | NSDQ: *step 1* (Caitlin)
1. Data Curation | Metro Names: *step 2* (Celina)
1. Analysis | Tradeoff Analysis: *step 3* (Runzi)
1. Analysis | Preprocessing and Exploration: *step 4* (Oliver)
1. Analysis | Signatures: *step 5* (Runzi + Oliver)
1. Data Curation | Watershed Boundaries: *step 6* (Matt)
1. Data Curation | Climate Data: *step 7* (Celina + Caitlin)
1. Data Curation | Watershed Attributes: *step 8* (Runzi)
1. Analysis | Typologies: *step 9* (Runzi + Oliver)

### I.ii Data Curation 

- **Step 1: NSDQ**
- **Step 2: Metro Names** 
- **Step 6: Watershed Boundaries**
- **Step 7: Climate Data**
- **Step 8: Watershed Attributes**

### I.iii Analysis

- **Step 3: Tradeoff Analysis** generates input data for signature analysis
- **Step 4: Preprocessing + Exploration** preliminary steps for determining pollutant relationships across stormwater events
- **Step 5: Signatures** results in clusters of stormwater events based on top-down and bottom-up analysis of pollutants 
- **Step 9: Typologies** relationships between signature clusters vs watersheds and climate data

## II. Data Curation

### I.i signature Analysis 

Requires **Steps 1 and 2:** NSQD and Metro Names.

### I.ii Typologies Analysis 

Requires **Steps 6, 7, and 8:** Watershed Boundaries, Climate Data, and Watershed Attributes.

## III. Analysis 

**Step 3: Tradeoff Analysis**
**Step 4: Preprocessing and Exploration**
**Step 5: Signatures**
**Step 9: Typologies**

```mermaid
graph TD;
        step1_and_2-->step3a;
        step1_and_2-->step3b;
        step1_and_2-->step3c_Res;
        step1_and_2-->step3d_Indust;
        step3a-->NAT_ANALYSIS;
        step3b-->NAT_ANALYSIS;
        step3c-->METRO_ANALYSIS;
        step3d-->METRO_ANALYSIS;
```

## IV. Markdown Examples 

Here are some flavored markdown examples that GitLab uses natively. 

### IV.i Graphs using Mermaid Markdown

```mermaid
graph TD;
      A-->B;
      A-->C;
      B-->D;
      C-->D;     
``` 

### IV.ii Task Lists 

1. [x] Completed Task 
1. [ ] Uh oh! 
   1. [ ] Sub-task Uh Oh! 1
   1. [x] Sub-task Uh Oh! 2

 