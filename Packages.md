# Notebook Workflows

This website provides **the iciest** coding practices for collaborative data-driven / analytical research projects and software development. 

Documentation can be [found here (*link*)](https://thru-echoes.gitlab.io/echoes-coding/Notebooks.md).

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [I. What is the minimum number of Notebooks for our workflow?](#i-what-is-the-minimum-number-of-notebooks-for-our-workflow)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## I. What is the minimum number of Notebooks for our workflow? 

That still capture all of the analytical steps? 

